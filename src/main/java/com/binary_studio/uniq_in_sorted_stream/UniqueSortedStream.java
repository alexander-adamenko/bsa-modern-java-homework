package com.binary_studio.uniq_in_sorted_stream;

import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		AtomicLong LastId = new AtomicLong(0);
		return stream.filter(x -> x.getPrimaryId() > LastId.getAndSet(x.getPrimaryId()));
	}

}
