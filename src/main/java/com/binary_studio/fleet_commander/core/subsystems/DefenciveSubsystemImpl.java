package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger powerGridConsumption;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		try {
			if (name == null || name.charAt(0) == ' ') {
				throw new IllegalArgumentException("Name should be not null and not empty");
			}

			return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent,
					shieldRegeneration, hullRegeneration);
		}
		catch (IllegalArgumentException exception) {
			throw exception;
		}
	}

	public DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
		this.name = name;
		this.powerGridConsumption = powergridConsumption;
		this.capacitorConsumption = capacitorConsumption;
		this.impactReductionPercent = impactReductionPercent;
		this.shieldRegen = shieldRegeneration;
		this.hullRegen = hullRegeneration;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		if (this.impactReductionPercent.value() > 95) {
			return new AttackAction(PositiveInteger.of((int) Math.ceil(incomingDamage.damage.value() * (5d / 100d))),
					incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
		}
		else {
			return new AttackAction(
					PositiveInteger.of((int) Math.ceil(incomingDamage.damage.value()
							* ((double) (100 - this.impactReductionPercent.value()) / 100d))),
					incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
		}

	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
