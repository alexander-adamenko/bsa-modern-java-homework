package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger powerGridConsumption;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		try {
			if (name == null || name.charAt(0) == ' ') {
				throw new IllegalArgumentException("Name should be not null and not empty");
			}

			return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
					baseDamage);
		}
		catch (IllegalArgumentException exception) {
			throw exception;
		}
	}

	public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powerGridConsumption = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	@Override
	public PositiveInteger getBaseDamage() {
		return this.baseDamage;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier;
		if (target.getSize().value() >= this.optimalSize.value()) {
			sizeReductionModifier = 1;
		}
		else {
			sizeReductionModifier = (float) target.getSize().value() / (float) this.optimalSize.value();
		}
		double speedReductionModifier;
		if (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) {
			speedReductionModifier = 1;
		}
		else {
			speedReductionModifier = (float) this.optimalSpeed.value() / (2 * (float) target.getCurrentSpeed().value());
		}
		Integer damage = (int) Math
				.ceil(this.baseDamage.value() * (Math.min(sizeReductionModifier, speedReductionModifier)));
		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
