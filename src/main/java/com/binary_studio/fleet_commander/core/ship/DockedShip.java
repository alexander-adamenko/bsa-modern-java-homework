package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger powerGrid;

	private PositiveInteger freePowerGrid;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		try {
			if (name == null || name.charAt(0) == ' ') {
				throw new IllegalArgumentException("Name should be not null and not empty");
			}

			return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate,
					speed, size);
		}
		catch (IllegalArgumentException exception) {
			throw exception;
		}
	}

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powerGrid = powergridOutput;
		this.freePowerGrid = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRegeneration = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		try {
			if (subsystem == null && this.attackSubsystem != null) {
				this.freePowerGrid = PositiveInteger
						.of(this.freePowerGrid.value() + this.attackSubsystem.getPowerGridConsumption().value());
				this.attackSubsystem = null;
			}
			else {
				if (subsystem.getPowerGridConsumption().value() > this.freePowerGrid.value()) {
					throw new InsufficientPowergridException(
							subsystem.getPowerGridConsumption().value() - this.powerGrid.value());
				}
				this.attackSubsystem = subsystem;
				this.freePowerGrid = PositiveInteger
						.of(this.freePowerGrid.value() - subsystem.getPowerGridConsumption().value());
			}

		}
		catch (InsufficientPowergridException exception) {
			throw exception;
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		try {
			if (subsystem == null && this.defenciveSubsystem != null) {
				this.freePowerGrid = PositiveInteger
						.of(this.freePowerGrid.value() + this.defenciveSubsystem.getPowerGridConsumption().value());
				this.defenciveSubsystem = null;
			}
			else {
				if (subsystem.getPowerGridConsumption().value() > this.freePowerGrid.value()) {
					throw new InsufficientPowergridException(
							subsystem.getPowerGridConsumption().value() - this.powerGrid.value());
				}
				this.defenciveSubsystem = subsystem;
				this.freePowerGrid = PositiveInteger
						.of(this.freePowerGrid.value() - subsystem.getPowerGridConsumption().value());
			}
		}
		catch (InsufficientPowergridException exception) {
			throw exception;
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		try {
			if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
				throw NotAllSubsystemsFitted.bothMissing();
			}
			if (this.attackSubsystem == null) {
				throw NotAllSubsystemsFitted.attackMissing();
			}
			if (this.defenciveSubsystem == null) {
				throw NotAllSubsystemsFitted.defenciveMissing();
			}
			return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitorAmount,
					this.capacitorRegeneration, this.powerGrid, this.freePowerGrid, this.speed, this.size,
					this.attackSubsystem, this.defenciveSubsystem);
		}
		catch (NotAllSubsystemsFitted exception) {
			throw exception;
		}

	}

}
