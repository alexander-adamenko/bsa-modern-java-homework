package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger currentShieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger currentHullHP;

	private PositiveInteger capacitorAmount;

	private PositiveInteger currentCapacitorAmount;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger powerGrid;

	private PositiveInteger freePowerGrid;

	private PositiveInteger currentSpeed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRegeneration, PositiveInteger powerGrid,
			PositiveInteger freePowerGrid, PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.currentShieldHP = shieldHP;
		this.hullHP = hullHP;
		this.currentHullHP = hullHP;
		this.powerGrid = powerGrid;
		this.freePowerGrid = freePowerGrid;
		this.capacitorAmount = capacitorAmount;
		this.currentCapacitorAmount = capacitorAmount;
		this.capacitorRegeneration = capacitorRegeneration;
		this.currentSpeed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		this.currentCapacitorAmount = PositiveInteger
				.of(this.currentCapacitorAmount.value() + this.capacitorRegeneration.value());
		if (this.currentCapacitorAmount.value() > this.capacitorAmount.value()) {
			this.currentCapacitorAmount = PositiveInteger.of(this.capacitorAmount.value());
		}

	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.currentSpeed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.currentCapacitorAmount.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		else {
			this.currentCapacitorAmount = PositiveInteger
					.of(this.currentCapacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
			return Optional
					.of(new AttackAction(this.attackSubsystem.getBaseDamage(), this, target, this.attackSubsystem));
		}

	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		if ((this.currentShieldHP.value() - this.defenciveSubsystem.reduceDamage(attack).damage.value()) > 0) {
			this.currentShieldHP = PositiveInteger
					.of(this.currentShieldHP.value() - this.defenciveSubsystem.reduceDamage(attack).damage.value());
		}
		else {
			if (this.currentShieldHP.value() + this.currentHullHP.value()
					- this.defenciveSubsystem.reduceDamage(attack).damage.value() > 0) {
				this.currentShieldHP = PositiveInteger.of(0);
				this.currentHullHP = PositiveInteger.of(this.currentShieldHP.value() + this.currentHullHP.value()
						- this.defenciveSubsystem.reduceDamage(attack).damage.value());
				return new AttackResult.DamageRecived(attack.weapon,
						this.defenciveSubsystem.reduceDamage(attack).damage, attack.target);
			}
			else {
				this.currentShieldHP = PositiveInteger.of(0);
				this.currentHullHP = PositiveInteger.of(0);
				return new AttackResult.Destroyed();
			}
		}
		return new AttackResult.DamageRecived(attack.weapon, this.defenciveSubsystem.reduceDamage(attack).damage,
				attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.currentCapacitorAmount.value() < this.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		else {
			this.currentCapacitorAmount = PositiveInteger.of(
					this.currentCapacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
			PositiveInteger hullHPDiff = PositiveInteger.of(this.currentHullHP.value());
			PositiveInteger shieldHPDiff = PositiveInteger.of(this.currentShieldHP.value());
			this.currentHullHP = PositiveInteger
					.of(this.currentHullHP.value() + this.defenciveSubsystem.regenerate().hullHPRegenerated.value());
			this.currentShieldHP = PositiveInteger.of(
					this.currentShieldHP.value() + this.defenciveSubsystem.regenerate().shieldHPRegenerated.value());

			if (this.currentHullHP.value() > this.hullHP.value()) {
				this.currentHullHP = PositiveInteger.of(this.hullHP.value());
			}
			if (this.currentShieldHP.value() > this.shieldHP.value()) {
				this.currentShieldHP = PositiveInteger.of(this.shieldHP.value());
			}
			hullHPDiff = PositiveInteger.of(this.currentHullHP.value() - hullHPDiff.value());
			shieldHPDiff = PositiveInteger.of(this.currentShieldHP.value() - shieldHPDiff.value());
			return Optional.of(new RegenerateAction(shieldHPDiff, hullHPDiff));
		}
	}

}
