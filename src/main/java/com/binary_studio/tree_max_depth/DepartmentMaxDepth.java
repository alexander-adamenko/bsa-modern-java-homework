package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.List;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		int currentDepthIndex = 0;
		int maxDepth = 1;
		List<Integer> IndexOfCurrentSubDepartment = new ArrayList<>();
		Department currentDepartment = new Department(rootDepartment);
		IndexOfCurrentSubDepartment.add(0);
		while (true) {
			assert currentDepartment != null;
			if (currentDepartment.hasSubDepartmentByIndex(IndexOfCurrentSubDepartment.get(currentDepthIndex))) {
				if (currentDepartment
						.hasNotNullSubDepartmentByIndex(IndexOfCurrentSubDepartment.get(currentDepthIndex))) {
					currentDepartment = currentDepartment
							.getSubDepartmentByIndex(IndexOfCurrentSubDepartment.get(currentDepthIndex));
					IndexOfCurrentSubDepartment.set(currentDepthIndex,
							IndexOfCurrentSubDepartment.get(currentDepthIndex) + 1);
					currentDepthIndex++;
					IndexOfCurrentSubDepartment.add(0);
					if (maxDepth - 1 < currentDepthIndex) {
						maxDepth = currentDepthIndex + 1;
					}

				}
				else {
					IndexOfCurrentSubDepartment.set(currentDepthIndex,
							IndexOfCurrentSubDepartment.get(currentDepthIndex) + 1);
				}
			}
			else {
				if (currentDepartment.parentDepartment != null) {
					currentDepartment = currentDepartment.parentDepartment;
					currentDepthIndex--;
				}
				else {
					break;
				}
			}
		}
		return maxDepth;
	}

}
