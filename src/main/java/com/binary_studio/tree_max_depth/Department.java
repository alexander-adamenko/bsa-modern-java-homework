package com.binary_studio.tree_max_depth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Department {

	public final String name;

	public final List<Department> subDepartments;

	public Department parentDepartment;

	public boolean hasSubDepartmentByIndex(Integer s) {
		return s < this.subDepartments.size();
	}

	public boolean hasNotNullSubDepartmentByIndex(Integer s) {
		if (s < this.subDepartments.size()) {
			return this.subDepartments.get(s) != null;
		}
		return false;
	}

	@Nullable
	public Department getSubDepartmentByIndex(Integer s) {
		if (s < this.subDepartments.size()) {
			return this.subDepartments.get(s);
		}
		return null;
	}

	public Department(@NotNull Department department) {
		this.name = department.name;
		this.subDepartments = department.subDepartments;
		if (department.parentDepartment != null) {
			this.parentDepartment = new Department(department.parentDepartment);
		}
		else {
			this.parentDepartment = null;
		}

	}

	public Department(String name) {
		this.name = name;
		this.subDepartments = new ArrayList<>();
		this.parentDepartment = null;
	}

	public Department(String name, Department... departments) {
		this.name = name;
		this.parentDepartment = null;
		this.subDepartments = Arrays.asList(departments);
		for (Department subDepartment : this.subDepartments) {
			if (subDepartment != null) {
				subDepartment.parentDepartment = this;
			}
		}
	}

}
